package me.synergy.controllers;

import me.synergy.models.Blog;
import me.synergy.models.MockData;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class BlogController {

    MockData mockData = MockData.getInstance();

    @RequestMapping("/")
    public String index(){
        return "congratulations";
    }

    @GetMapping("/blogs")
    public List<Blog> getBLogs(){
        return mockData.getBlogData();
    }

    @GetMapping("/blogs/{id}")
    public Blog getBlogById(@PathVariable String id){
        int blogId = Integer.parseInt(id);
        return mockData.getBlogById(blogId);
    }

    @PostMapping("/blogs/search")
    public List<Blog> searchBlogs(@RequestBody Map<String, String> body){
        String searchStream = body.get("text");
        return mockData.searchBlogByText(searchStream);
    }

    @PostMapping("/blog")
    public Blog create(@RequestBody Map<String, String> body){
        int id = Integer.parseInt(body.get("id"));
        String title = body.get("title");
        String content = body.get("content");
        return mockData.createBlog(id, title, content);
    }

    @PutMapping("/blog/{id}")
    public Blog update(@PathVariable String id, @RequestBody Map<String, String> body){
        int blogId = Integer.parseInt(id);
        String title = body.get("title");
        String content = body.get("content");
        return mockData.updateBLog(blogId, title, content);
    }

    @DeleteMapping("blog/{id}")
    public boolean delete(@PathVariable String id){
        int blogId = Integer.parseInt(id);
        return mockData.deletBlogByID(blogId);
    }
}
